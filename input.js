var recognition,
    input,
    timer,
    patience = 1000;//low patience might interrupt you

var said = false;

function speak(output) {
    console.log(output);
    var message = encodeURIComponent(output),
        //url = 'https://translate.google.com/translate_tts?tl=en&q=' + message;//<--taal bij &tl
    url = 'http://tts-api.com/tts.mp3?q=' + message; //<-- ajax call, link terugkrijgen
        //url = 'http://media.tts-api.com/c884fd7d09c82ad7c9d4d608cf0848f20c252bb4.mp3';
    $('#audio').attr('src', url)[0].play();
    console.log(url);
}

function handleInput() {
    console.log(input);

    if (input.indexOf("hello") > -1 && said == false)
    {
        $('#audio')[0].play();
        said = true;
    }

    input = null;
}

function init() {
    recognition = new webkitSpeechRecognition();
    recognition.lang = "en-GB";
    recognition.continuous = true;
    recognition.interimResults = true;
    recognition.onresult = function(event) {
        clearTimeout(timer);
        input = event.results[event.results.length - 1][0].transcript;
        timer = setTimeout(handleInput, patience);
    };
    recognition.start();

    speak("hello there");
}

window.addEventListener('load', init);